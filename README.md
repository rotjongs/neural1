# Experiment with Neural Network
This is the code from the neural network tutorial on
https://www.youtube.com/watch?v=XqRUHEeiyCs by Ryan Harris

We cleaned the code a bit to make it work:

# Future work
* Get rid of all pip-8 non compliances
* get it to run in a dockerized flask application
* get graphics attached

# Way of working
* We store the code in BitBucket in https://bitbucket.org/brainstam/neural1
* Changes are made by branching from the development branch
* When pushing back make a pull request (merge request) to the development branch from your feature or bug branch
* merge to master on deploy of the final application

# Environment
We developed this using Python 3.6 in a virtualenv in Ubuntu 16.04.3 LTS
