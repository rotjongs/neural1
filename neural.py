"""Neural network module"""
import numpy as np

# transfer functions


def sgm(x, derivative=False):
    if not derivative:
        return 1.0 / (1.0 + np.exp(-x))
    else:
        out = sgm(x)
        return out * (1.0 - out)


def linear(x, derivative=False):
    if not derivative:
        return x
    else:
        return 1.0


def gaussian(x, derivative=False):
    if not derivative:
        return np.exp(-x ** 2)
    else:
        return -2 * x * np.exp(-x ** 2)


def tanh(x, derivative=False):
    if not derivative:
        return np.tanh(x)
    else:
        return 1.0 - np.tanh(x) ** 2


class BackProgagationNetwork:
    """
    A back-propagation network
    """
    layer_count = 0
    shape = None
    weights = []
    t_funcs = []

    def __init__(self, layer_size, layer_functions=None):
        """Initialize the network"""

        # Layer info
        self.layer_count = len(layer_size) - 1
        self.shape = layer_size

        if layer_functions is None:
            l_funcs = []
            for layer in range(self.layer_count):
                if layer == self.layer_count - 1:
                    l_funcs.append(linear)
                else:
                    l_funcs.append(sgm)

        else:
            if len(layer_size) != len(layer_functions):
                raise ValueError("Incompatible list of transfer functions.")
            elif layer_functions[0] is not None:
                raise ValueError("Input layer cannot hava a transfer function.")
            else:
                l_funcs = layer_functions[1:]

        self.t_funcs = l_funcs

        # Data from last Run
        self._layerInput = []
        self._layer_output = []
        self._previousWeightDelta = []

        # Create the weight arrays
        for (l1, l2) in zip(layer_size[:-1], layer_size[1:]):
            self.weights.append(np.random.normal(scale=0.1, size=(l2, l1+1)))
            self._previousWeightDelta.append(np.zeros((l2, l1 + 1)))

    def run(self, input_set):
        """Run the network based on the input data"""

        ln_cases = input_set.shape[0]

        # Clear out the previous intermediate valiue lists
        self._layerInput = []
        self._layer_output = []

        # Run it!
        for index in range(self.layer_count):
            if index == 0:
                layer_input = self.weights[0].dot(np.vstack([input_set.T, np.ones([1, ln_cases])]))
            else:
                layer_input = self.weights[index].dot(np.vstack([self._layer_output[-1], np.ones([1, ln_cases])]))
            self._layerInput.append(layer_input)
            self._layer_output.append(self.t_funcs[index](layer_input))

        return self._layer_output[-1].T

    #
    # train_epoch method
    #

    def train_epoch(self, input_network, target, training_rate=0.2, momentum=0.5):
        """This method trains the network for one epoch"""

        delta = []
        ln_cases = input_network.shape[0]
        error = None

        # first run the network
        self.run(input_network)

        # Calculate our deltas
        for index in reversed(range(self.layer_count)):
            if index == self.layer_count - 1:
                # Compare to the target values
                output_delta = self._layer_output[index] - target.T
                error = np.sum(output_delta**2)
                delta.append(output_delta * self.t_funcs[index](self._layerInput[index], True))
            else:
                # Compare to the following layer's delta
                delta_pullback = self.weights[index + 1].T.dot(delta[-1])
                delta.append(delta_pullback[:-1, :] * self.t_funcs[index](self._layerInput[index], True))

        # Compute weight delta
        for index in range(self.layer_count):
            delta_index = self.layer_count - 1 - index

            if index == 0:
                layer_output = np.vstack([input_network.T, np.ones([1, ln_cases])])
            else:
                layer_output = np.vstack([self._layer_output[index - 1],
                                         np.ones([1, self._layer_output[index - 1].shape[1]])])

            cur_weight_delta = np.sum(layer_output[None, :, :].transpose(2, 0, 1) *
                                      delta[delta_index][None, :, :].transpose(2, 1, 0), axis=0)

            weight_delta = training_rate * cur_weight_delta + momentum * self._previousWeightDelta[index]

            self.weights[index] -= weight_delta

            self._previousWeightDelta[index] = weight_delta
        return error

    # Transfer functions


#
# If run as a script
#


if __name__ == "__main__":

    lvInput = np.array([[0, 0], [1, 0], [0, 1], [1, 1]])
    lvTarget = np.array([[0.0], [0.0], [1.0], [0.1]])
    lFuncs = [None, sgm, sgm]

    bpn = BackProgagationNetwork((2, 6, 1), lFuncs)

    lnMax = 400000
    lnErr = 1e-5
    for i in range(lnMax + 1):
        err = bpn.train_epoch(lvInput, lvTarget, momentum=0.7)
        if i % 10000 == 0:
            print("Iteration {0}\tError: {1:0.6f}".format(i, err))
        if err <= lnErr:
            print("Minimum error reached at iteration {0}".format(i))
            break

    # display output
    lvOutput = bpn.run(lvInput)
    for i in range(lvInput.shape[0]):
        print("Input: {0} Output: {1}".format(lvInput[i], lvOutput[i]))
